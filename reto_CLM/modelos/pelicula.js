var mongoose = require('mongoose'),
   Schema = mongoose.Schema
  
const peliSchema = new Schema({
    title: {
        type: String,
        required: true,
    },
    year: {
        type: Number,
        required: true
    },
    released: {
        type: String,
        required: true
    },
    genre: {
        type: String,
        required: true
    },
    actors: {
        type: String,
        required: true
    },
    director: {
        type: String,
        required: true
    },
    plot: {
        type: String,
        required: true
    },
    ratings: {
        type: [],
        required: true
    },
    createdAt: {
        type: Date,
        default: Date.now
    }
})

module.exports = mongoose.model('pelicula', peliSchema)