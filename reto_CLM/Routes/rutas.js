const Router = require('koa-router')
const comb_Routers = require('koa-combine-routers')
const nameyear = require('./get_nameyear')
const todaspelis = require('./all_pelis')
const reemplazo = require('./post_cambio')
const mrouter = new Router()


//Definiendo rutas de búsqueda
mrouter.get('/', nameyear); //http://localhost:3000/?name={It}&year=2017
mrouter.get('/todas', todaspelis); //http://localhost:3000/todas?page=1
mrouter.post('/cambio', reemplazo); //http://localhost:3000/cambio
const router = comb_Routers(
    mrouter
)
module.exports = router;