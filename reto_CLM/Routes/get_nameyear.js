const koa = require('koa');
const koarouter = require('koa-router');
const Movie = require("../modelos/pelicula");
const axios = require("axios"); 
const app = new koa();

const busquedad_name_year = async (ctx) => {
  try {
          let arr = [];
          const { name } = ctx.query; //para que funcione http://localhost:3000/pelicula?name={nombre de la pelicula}&year=2006
          const year = ctx.headers.year;
         
          await axios.get(`https://www.omdbapi.com/?apikey=f7e3873b&t=${name}&y=${year}`).then((res) => {
            arr.push(res.data); //elementos añadir a res.data
        });
        const movie = await Movie.findOne({ title: arr[0].Title }); //llamar a Schema
        //guarduar la pelicula una sola vez
        if (!movie) { 
            const newMovie = new Movie({
                title: arr[0].Title,
                year: arr[0].Year,
                released: arr[0].Released,
                plot: arr[0].Plot,
                actors: arr[0].Actors,
                genre: arr[0].Genre,
                director: arr[0].Director,
                ratings: arr[0].Ratings.concat.apply(arr[0].Ratings.map((elem) => elem.Value + " , " + elem.Source))
            });

              newMovie.save();
              ctx.body = newMovie;
        }else{
              ctx.body = movie;
        }
  }catch (err) { //fin try - Validación de Entradas
        ctx.status = 403;
        ctx.body = { message: 'Sin datos' };
  }
};

module.exports = busquedad_name_year;
