const koa = require('koa');
const Movie = require("../modelos/pelicula");
const bodyParser = require('koa-bodyparser');
const Router = require('koa-router');
const data = require('./envio');
 
var app = new koa();
app.use(bodyParser());

const post = async (ctx) => {

const obj = ctx.request.body; //console.log(obj);//Debe llegar {movie : "Star Wars", find : "jedi", replace : "CLM Dev"};

try {
                 var replace = obj.replace; //console.log(replace);
                 await Movie.find({ title: obj.movie, plot:obj.find}); //filtro busquedad titulo star wars junto a plot jedi 
                 await Movie.updateMany({title: obj.movie, plot:obj.find}, {$set: {plot: obj.replace}}); //remplazo plot de la busquedad anterior por CLM Dev
               
                 ctx.body = {
                  feature: await Movie.find()
                };
     

} catch (err) {
  ctx.status = 403;
    ctx.body = { message: 'Sin datos' };
}
};

module.exports = post;

