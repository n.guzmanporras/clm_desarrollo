const koa = require('koa');
const Movie = require("../modelos/pelicula");
const mongoose = require('mongoose');
const app = new koa();


const all_pelis = async (ctx) => {
  try {
        const {page} = ctx.query;
        const limit = 5;
       
        const skip = (page-1)* limit; //console.log(skip);
          const movies = await Movie.find() //recupero todos los documentos almacenados en la colección peliculas
          .skip(skip) //Skip paginación - pagina 1: (1-1)*5 = 0*5 = 0
          .limit(limit); //tamaño de la pagina = 5
        const count = await Movie.countDocuments(); //console.log(movies)//6 
        if(movies.length == 0){
        
        ctx.body= 'No hay Datos - Indique una página menor ';
        }else{
          ctx.body = { movies, TOTAL_PAGINACIÓN: Math.ceil(count / limit), pagina_actual:page };
        }
        
  }catch (err) {
    ctx.status = 403;
    ctx.body = { message: 'Sin datos' };
  }

};

module.exports = all_pelis;
