const Koa = require('koa');
const koaBody = require('koa-body');
const json = require('koa-json');
const mongoose = require('mongoose');
const router = require('./Routes/rutas');

const app = new Koa();

const PORT = 3000

mongoose.connect('mongodb+srv://reto_clmdb:Clm30559378@cluster0.6uqyq.mongodb.net/clm_rdb?retryWrites=true&w=majority');

mongoose.connection.on('error', (err) => {
  console.log(err);
});


//Middleware to parse the body of the request
app.use(koaBody());

//Route middleware
app.use(router());


//Middleware to make the json prettier
app.use(json());

app.listen(PORT, () => {
  console.log(3000,'Puerto escuchando');
});

